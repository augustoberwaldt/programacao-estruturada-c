#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

struct Arvore {
    char  valor[30];
    struct Arvore * esq;
    struct Arvore * dir;
	int pergunta;
};

typedef struct  Arvore ArvoreBinario;

ArvoreBinario*  criarArvore()
{
    return NULL;
}

ArvoreBinario*  criarArvore2(  char valor[],
                               ArvoreBinario *esq,
                               ArvoreBinario *dir,
							   int pergunta
                             )
{
ArvoreBinario *raiz  =  (ArvoreBinario*) malloc(sizeof(ArvoreBinario));
    strcpy(raiz->valor, valor);
    raiz->pergunta = pergunta;
	raiz->esq   = esq;
    raiz->dir   = dir;
    return raiz;
}

void imprimiArvore(ArvoreBinario *raiz)
{
   if (raiz != NULL) {
       printf("%s \n", raiz->valor);
       imprimiArvore(raiz->esq);
       imprimiArvore(raiz->dir);
   }

}

void montaPerguntas(ArvoreBinario *raiz)
{
	int opcao;
	if (raiz != NULL) {
		if (raiz->pergunta == 1) {
            printf("%s\n", raiz->valor);
            scanf("%i",&opcao);
            if (opcao == 1) {
                  montaPerguntas(raiz->esq);
            } else  {
                  montaPerguntas(raiz->dir);    
            }
		} else {
            printf("%s\n", raiz->valor); 
        } 
   }	
}


void main ()
{   

  
    ArvoreBinario* grupo0 = criarArvore2("O seu continente e America do Central ?", 
											criarArvore2("O seu continente e America do Central!! ", criarArvore(),criarArvore(),  FALSE), 
											criarArvore2("Seu continente e america do Norte !! ", criarArvore(),criarArvore(),  FALSE),
										    TRUE
										);
    
    
    ArvoreBinario* grupo1 = criarArvore2("O seu continente e America do Sul ?", 
											criarArvore2("Seu continente e america do sul   !! ", criarArvore(),criarArvore(),  FALSE), 
											grupo0,
										    TRUE
										);
    
										
	ArvoreBinario* grupo3 = criarArvore2("O seu continente Africa ?", 
											criarArvore2("O seu continente Africa  !! ", criarArvore(),criarArvore(),FALSE), 
											criarArvore2("Seu continente e Asia   !! ", criarArvore(),criarArvore(),FALSE),
											TRUE
										);
	
	ArvoreBinario* grupo2 = criarArvore2("O seu continente e Europa ?", 
											criarArvore2("Seu continente Europa   !! ", criarArvore(),criarArvore() ,FALSE), 
											grupo3,
											TRUE
										);
	  
    
    ArvoreBinario* arvore  =  criarArvore2("Voce e do continente americano ?" , grupo1  , grupo2 ,TRUE );

   
    montaPerguntas(arvore);
   
   
   
   system("pause");
   
   

}
