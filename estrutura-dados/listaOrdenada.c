#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct Nodo{
    int  elemento;
    struct  Nodo*  proximo;
}Lista;


void  insertElemento(int valor , Lista** head)
{
   Lista*  aux  = NULL;
   Lista*  ant  = NULL;

   if (*head == NULL) {
       *head    =  (Lista*) malloc(sizeof(Lista));
       (*head)->elemento = valor;
       (*head)->proximo  = NULL;
        return  ;
   }

   if (valor < (*head)->elemento) {
        aux = (Lista*) malloc(sizeof(Lista));
        aux->elemento = valor;
        aux->proximo  = (*head);
        (*head)   = aux;
   }
   if ((*head)->proximo == NULL) {
        (*head)->proximo = (Lista*) malloc(sizeof(Lista));
        (*head)->proximo = valor ;
        (*head)->proximo->proximo  = NULL ;
        return ;
   }

   aux = (*head)->proximo;
   ant =(*head);
   while(aux->proximo !=NULL  && aux->elemento < valor) {
        ant = aux;
        aux = aux->proximo;

   }
   if (aux->proximo == NULL  && aux->elemento < valor ) {
     aux->proximo = (Lista*) malloc(sizeof(Lista));
     aux->proximo->elemento = valor;
     aux->proximo->proximo = NULL;
     return ;
   }
   ant->proximo = (Lista*) malloc(sizeof(Lista));
   ant->proximo->elemento = valor;
   ant->proximo =aux;

}
void main ()
{

    Lista* lista = NULL;
    Lista*  aux  = NULL;

    insertElemento(9,&lista);
    insertElemento(8,&lista);
    insertElemento(7,&lista);
    insertElemento(6,&lista);


    aux = lista ;

    while(aux != NULL) {
        printf("%d \n", aux->elemento);
        aux= aux->proximo;
    }


}

