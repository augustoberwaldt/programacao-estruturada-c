#include <stdio.h>

struct Arvore {
    char  valor;
    struct Arvore * esq;
    struct Arvore * dir;
};

typedef struct  Arvore ArvoreBinario;

ArvoreBinario*  criarArvore()
{
    return NULL;
}

ArvoreBinario*  criarArvore2(  char valor,
                               ArvoreBinario *esq,
                               ArvoreBinario *dir
                             )
{
ArvoreBinario *raiz  =  (ArvoreBinario*) malloc(sizeof(ArvoreBinario));
    raiz->valor = valor;
    raiz->esq   = esq;
    raiz->dir   = dir;
    return raiz;
}

void imprimiArvore(ArvoreBinario *raiz)
{
   if (raiz != NULL) {
       printf("%c \n", raiz->valor);
       imprimiArvore(raiz->esq);
       imprimiArvore(raiz->dir);
   }

}

void main ()
{

    ArvoreBinario* arvore1  =  criarArvore2('a' , criarArvore2('b', criarArvore(), criarArvore()),  criarArvore2('c', criarArvore(), criarArvore()) );

    imprimiArvore(arvore1);

}
#include <stdio.h>

struct Arvore {
    char  valor;
    struct Arvore * esq;
    struct Arvore * dir;
};

typedef struct  Arvore ArvoreBinario;

ArvoreBinario*  criarArvore()
{
    return NULL;
}

ArvoreBinario*  criarArvore2(  char valor,
                               ArvoreBinario *esq,
                               ArvoreBinario *dir
                             )
{
ArvoreBinario *raiz  =  (ArvoreBinario*) malloc(sizeof(ArvoreBinario));
    raiz->valor = valor;
    raiz->esq   = esq;
    raiz->dir   = dir;
    return raiz;
}

void imprimiArvore(ArvoreBinario *raiz)
{
   if (raiz != NULL) {
       printf("%c \n", raiz->valor);
       imprimiArvore(raiz->esq);
       imprimiArvore(raiz->dir);
   }

}

void main ()
{

    ArvoreBinario* arvore1  =  criarArvore2('a' , criarArvore2('b', criarArvore(), criarArvore()),  criarArvore2('c', criarArvore(), criarArvore()) );

    imprimiArvore(arvore1);

}
